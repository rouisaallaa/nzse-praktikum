package com.example.parkplatz;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Details_TicketVerkaufen extends AppCompatActivity implements AdapterView.OnItemSelectedListener{
    private List<Angebot> allAngebote;
    private RecyclerView recyclerView;
    private Button Add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details__ticket_verkaufen);
        recyclerView = findViewById(R.id.Recycler_Angebot);
        //die elemente im RecyclerView werden mit hilfe eines linear layout managers angeordnet
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);


        Add = findViewById(R.id.button_fertig);

        Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),MainActivityTicketVerkauf.class);
                startActivity(intent);
            }
        });

    }

    private void fillAngebote() {
        List<Angebot> filteredAngebote;

        filteredAngebote=allAngebote;

        recyclerView.setAdapter(new TicketAdapter(this, filteredAngebote));

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        fillAngebote();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onResume(){
        super.onResume();
        //allAngebote = DatabaseClient.getInstance(getApplicationContext()).getAppDatabase().angebotDao().getAll();
        //fillAngebote();
        class LoadTask extends AsyncTask<Void, Void, List<Angebot>> {

            @Override
            protected List<Angebot> doInBackground(Void... voids) {
                return DatabaseClient.getInstance(getApplicationContext()).getAppDatabase().angebotDao().getAll();
            }

            @Override
            protected void onPostExecute(List<Angebot> angebots) {
                allAngebote = angebots;
                fillAngebote();
            }
        }
        LoadTask loadTask = new LoadTask();
        loadTask.execute();
    }


}
