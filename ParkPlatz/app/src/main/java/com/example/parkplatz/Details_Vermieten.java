package com.example.parkplatz;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

public class Details_Vermieten extends AppCompatActivity {
    private Angebot angebot = null;
    AppDatabase db;
    private EditText strasse ,ort,plz,dauerVon,dauerBis,preis;
    private ImageView imageViewCity;
    private static final int REQUEST_PICK_IMAGE = 42;
    private String imageUri = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_vermiet);
        strasse = (EditText)findViewById(R.id.strasseTextPlian);
        ort = (EditText)findViewById(R.id.ortTextPlian);
        plz = (EditText)findViewById(R.id.plzTextPlian);
        dauerVon = (EditText)findViewById(R.id.vonDate);
        dauerBis = (EditText)findViewById(R.id.bisDate);
        preis = (EditText)findViewById(R.id.preisTextPlian);

        Button btnDelete = findViewById(R.id.btnDelete);
        btnDelete.setVisibility(View.INVISIBLE);
        Button btnUpdate = (Button) findViewById(R.id.anbietBt);
        Button returnBt = (Button)findViewById(R.id.returnBt);
        btnUpdate.setOnClickListener(view -> saveItem());

        imageViewCity = findViewById(R.id.imageView);
        imageViewCity.setOnClickListener(view -> selectImage());
        returnBt.setOnClickListener(view->returnMethod());
        angebot = (Angebot) getIntent().getSerializableExtra("angebot");
        if (angebot != null){
            String Adresse = angebot.getAdresse();
            String Strasse = Adresse.substring(0,Adresse.indexOf(","));
            String Ort = Adresse.substring(Adresse.indexOf(",")+1,Adresse.indexOf("--"));
            String Plz = Adresse.substring(Adresse.indexOf("--")+2,Adresse.length()-1);


            strasse.setText( Strasse);
            ort.setText(Ort);
            plz.setText(Plz);
            preis.setText(angebot.getPreis());
            dauerVon.setText(angebot.getDauerVon());
            dauerBis.setText(angebot.getDauerBis());
            imageUri = angebot.getImageUri();
            Uri uri = Uri.parse(imageUri);
            if (uri != null)
                imageViewCity.setImageURI(uri);
            btnDelete.setVisibility(View.VISIBLE);
            btnDelete.setOnClickListener(view -> deleteItem());

        }

        db = DatabaseClient.getInstance(getApplicationContext()).getAppDatabase();



    }
    private void deleteItem() {
        if (angebot != null) {
            new Thread(() -> {
                db.angebotDao().delete(angebot);
                Details_Vermieten.this.runOnUiThread(() -> Toast.makeText(Details_Vermieten.this, "gelöscht", Toast.LENGTH_LONG).show());
            }).start();

        }
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main3, menu);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int itemId = item.getItemId() ;

        if(itemId == R.id.back ){
            Intent myIntent =  new Intent(this , MainActivity.class) ;
            startActivity(myIntent);
        }

        return super.onOptionsItemSelected(item);
    }
    private void saveItem(){
        if (angebot == null) {
            angebot = new Angebot();
        }
        if(strasse.getText().toString().isEmpty())
        {
            strasse.setError("Die Strasse ist angefordert!");
            strasse.requestFocus();
            return;
        }
        if(ort.getText().toString().isEmpty())
        {
            ort.setError("Der Ort ist angefordert!");
            ort.requestFocus();
            return;
        }
        if(plz.getText().toString().isEmpty())
        {
            plz.setError("Die Postleitzahl ist angefordert!");
            plz.requestFocus();
            return;
        }
        if(dauerVon.getText().toString().isEmpty())
        {
            dauerVon.setError("Das Datum ist angefordert!");
            dauerVon.requestFocus();
            return;
        }
        if(dauerBis.getText().toString().isEmpty())
        {
            dauerBis.setError("Das Datum ist angefordert!");
            dauerBis.requestFocus();
            return;
        }
        if(preis.getText().toString().isEmpty())
        {
            preis.setError("Der Preis ist angefordert!");
            preis.requestFocus();
            return;
        }
        angebot.setAdresse(strasse.getText().toString().trim()+", "+ort.getText().toString().trim()+"--"+plz.getText().toString().trim());
        angebot.setPreis(preis.getText().toString());
        angebot.setDauerVon(dauerVon.getText().toString());
        angebot.setDauerBis(dauerBis.getText().toString());

        angebot.setImageUri(imageUri);
        if (angebot.getUid() > 0)
            new Thread(() -> {db.angebotDao().update(angebot);}).start();
        else
            new Thread(() -> {db.angebotDao().insert(angebot);}).start();
        finish();
    }
    private void returnMethod() {
        Intent returns = new Intent(this, MainActivityParkplatzVermieten.class);
        startActivity(returns);

    }
    private void selectImage(){

        Intent gallery = new Intent(Intent.ACTION_OPEN_DOCUMENT, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, REQUEST_PICK_IMAGE);
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_PICK_IMAGE && data != null){
            Uri uri = data.getData();
            if (uri != null) {
                //sonst nach Neustart nicht mehr verfügbar ...
                getContentResolver().takePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                imageViewCity.setImageURI(uri);
                imageUri = uri.toString();
            }
        }
    }


}
