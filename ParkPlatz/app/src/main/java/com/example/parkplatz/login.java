package com.example.parkplatz;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.FirebaseError;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class login extends AppCompatActivity {

    private FirebaseDatabase aDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference mDatabase = aDatabase.getReference();
    User tmp;
    String dBpassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);
        final EditText mUserName = (EditText) findViewById(R.id.email);
        final EditText  mPasswordView = (EditText) findViewById(R.id.password);
        Button btnLogin = (Button) findViewById(R.id.login_button);
        Button btnRegister = (Button) findViewById(R.id.register_button);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String UserName = mUserName.getText().toString();
                String Pwd = mPasswordView.getText().toString();
                DatabaseReference myRef =mDatabase.child("users").child(UserName);

                    myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                tmp = dataSnapshot.getValue(User.class);
                                dBpassword = tmp.passwort;
                                if(Pwd.equals(dBpassword)){
                                    Intent myIntent;
                                    myIntent = new Intent(login.this , MainActivity.class);
                                    startActivity(myIntent);
                                    Toast.makeText(login.this,"You are signed in successfuly.", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(login.this,"Sorry, password is incorrect!", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(login.this,"Sorry, user name is incorrect!", Toast.LENGTH_LONG).show();
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            dBpassword = "Error";
                        }
                    });

            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent;
                myIntent = new Intent(login.this, registration.class);
                startActivity(myIntent);
            }
        });
    }
}
