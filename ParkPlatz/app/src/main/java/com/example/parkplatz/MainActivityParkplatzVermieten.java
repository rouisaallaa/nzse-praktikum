package com.example.parkplatz;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MainActivityParkplatzVermieten extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private List<Angebot> allAngebote = null;
    private RecyclerView recyclerView;
    private Button addButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vermiet);
        recyclerView = findViewById(R.id.Recycler_Angebot);
        //Layout für die Anordnung der Elemente
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);



        addButton = findViewById(R.id.addBt);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Details_Vermieten.class);
                startActivity(intent);
            }
        });
    }



    private void fillCities(){
        List<Angebot> filteredCities;

        filteredCities = allAngebote;


        recyclerView.setAdapter(new AngebotAdapter(this, filteredCities));
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        fillCities();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }




    @Override
    protected void onResume(){
        super.onResume();
       // allAngebote = DatabaseClient.getInstance(getApplicationContext()).getAppDatabase().angebotDao().getAll();
       // fillCities();
        class LoadTask extends AsyncTask<Void, Void, List<Angebot>> {

            @Override
            protected List<Angebot> doInBackground(Void... voids) {
                return DatabaseClient.getInstance(getApplicationContext()).getAppDatabase().angebotDao().getAll();
            }

            @Override
            protected void onPostExecute(List<Angebot> angebots) {
                allAngebote = angebots;
                fillCities();
            }
        }
        LoadTask loadTask = new LoadTask();
        loadTask.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main2, menu);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int itemId = item.getItemId() ;

        if(itemId == R.id.back ){
            Intent myIntent =  new Intent(this , MainActivity.class) ;
            startActivity(myIntent);
        }

        return super.onOptionsItemSelected(item);
    }
}
