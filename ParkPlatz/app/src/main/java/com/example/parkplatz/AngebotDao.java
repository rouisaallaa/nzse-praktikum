package com.example.parkplatz;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
interface AngebotDao {

    @Query("SELECT * FROM angebot")
    List<Angebot> getAll();

    @Insert
    long insert(Angebot angebot);

    @Delete
    void delete(Angebot angebot);

    @Update
    void update(Angebot angebot);
}
