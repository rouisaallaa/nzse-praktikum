package com.example.parkplatz;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class KaufUebersicht extends AppCompatActivity {
    private TextView strasse ,ort,plz,dauerVon,dauerBis,preis;

    private Angebot angebot = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kauf_uebersicht);
        angebot = (Angebot) getIntent().getSerializableExtra("angebot");

        strasse = (TextView)findViewById(R.id.editText_Adresse);
        String Adresse = angebot.getAdresse();
        String Strasse = Adresse.substring(0,Adresse.indexOf(","));
        strasse.setText( Strasse);

       ort = (TextView)findViewById(R.id.editText_Ort);
        String Ort = Adresse.substring(Adresse.indexOf(",")+1,Adresse.indexOf("--"));
        ort.setText(Ort);

        plz = (TextView)findViewById(R.id.editText_PLZ);
        dauerVon = (TextView)findViewById(R.id.editText_Von);
        dauerVon.setText(angebot.getDauerVon());

        dauerBis = (TextView)findViewById(R.id.editText_Bis);
        dauerBis.setText(angebot.getDauerBis());

        preis = (TextView)findViewById(R.id.editText_Preis);
        preis.setText(angebot.getPreis());




        String Plz = Adresse.substring(Adresse.indexOf("--")+2,Adresse.length()-1);
        plz.setText(Plz);




    }
}
