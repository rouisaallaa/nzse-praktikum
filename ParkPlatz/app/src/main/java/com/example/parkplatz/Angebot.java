package com.example.parkplatz;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class Angebot implements Serializable {
//    public Angebot(String adresse, int preis)

    @PrimaryKey(autoGenerate = true)
    private long uid;

    private String adresse;
    private String preis;
    private String dauerVon;
    private String imageUri;
    private String dauerBis;

    public long getUid() {
        return uid;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getPreis() {
        return preis;
    }

    public String getDauerVon() {
        return dauerVon;
    }

    public String getImageUri() {
        return imageUri;
    }

    public String getDauerBis() {
        return dauerBis;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setPreis(String preis) {
        this.preis = preis;
    }

    public void setDauerVon(String dauerVon) {
        this.dauerVon = dauerVon;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public void setDauerBis(String dauerBis) {
        this.dauerBis = dauerBis;
    }

    @Ignore
    public Angebot(String adresse, String preis, String dauerVon, String imageUri, String dauerBis) {
        setAdresse(adresse);
        setPreis(preis);
        setDauerVon(dauerVon);
        setDauerBis(dauerBis);
        setImageUri(imageUri);
    }

    public Angebot() {
    }

}
