package com.example.parkplatz;


import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;





import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class registration extends AppCompatActivity {

    private DatabaseReference mDatabase;
    Button btnRegistration;
    EditText email, pw, pwB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);

        mDatabase=FirebaseDatabase.getInstance().getReference();

        btnRegistration = (Button) findViewById(R.id.register_button);
        email = (EditText) findViewById(R.id.email);
        pw = (EditText) findViewById(R.id.password);
        pwB = (EditText) findViewById(R.id.passwordB);
        btnRegistration.setOnClickListener(view -> register());
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int itemId = item.getItemId() ;

        if(itemId == R.id.back ){
            Intent myIntent =  new Intent(this , login.class) ;
            startActivity(myIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void register(){
        if(email.getText().toString().isEmpty())
        {
            email.setError("Die E-Mail Adresse ist notwendig!");
            email.requestFocus();
            return;
        }

        if(pw.getText().toString().isEmpty())
        {
            pw.setError("Ein Passwort ist notwendig!");
            pw.requestFocus();
            return;
        }

        if(pwB.getText().toString().isEmpty())
        {
            pwB.setError("Bitte bestätigen sie das Passwort!");
            pwB.requestFocus();
            return;
        }

        if(!pw.getText().toString().equals(pwB.getText().toString())) {
            pwB.setError("Passwörter stimmen nicht überein!");
            pwB.requestFocus();
            return;
        }

        //Prüfung auf doppelten Username einbauen
        User user = new User(pw.getText().toString());
        mDatabase.child("users").child(email.getText().toString()).setValue(user);

        returnMethod();
    }

    private void returnMethod() {
        Intent returns = new Intent(this, login.class);
        startActivity(returns);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main2, menu);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        return true;
    }



}

