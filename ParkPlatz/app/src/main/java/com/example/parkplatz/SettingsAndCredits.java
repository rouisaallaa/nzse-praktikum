package com.example.parkplatz;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;

public class SettingsAndCredits extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings, menu);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int itemId = item.getItemId() ;

        if(itemId == R.id.back ){
            Intent myIntent =  new Intent(this , MainActivity.class) ;
            startActivity(myIntent);
        }

        return super.onOptionsItemSelected(item);
    }
}
