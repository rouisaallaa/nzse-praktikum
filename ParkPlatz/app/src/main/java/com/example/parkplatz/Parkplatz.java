package com.example.parkplatz;

import android.media.Image;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Parkplatz {

    public String adresse;
    public String ort;
    public String plz;
    public String von;
    public String bis;
    public String preis;
    public Image bild;


    public Parkplatz() {

    }

    public Parkplatz(String adresse, String ort, String plz, String von, String bis,String preis, Image bild) {
        this.adresse = adresse;
        this.ort = ort;
        this.plz = plz;
        this.von = von;
        this.bis = bis;
        this.preis = preis;
        this.bild = bild;
    }
}
