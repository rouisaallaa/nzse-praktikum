package com.example.parkplatz;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivityTicketVerkauf extends AppCompatActivity {
    private Button zurück,löschen,speichern;
    private Angebot angebot = null;
    AppDatabase db;
    private EditText adresse, ort, plz, dauerVon,dauerBis,preis;
    private ImageView imageViewCity;
    private static final int REQUEST_PICK_IMAGE = 42;
    private String imageUri ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        adresse =(EditText)findViewById(R.id.editText_Adresse);
        ort=(EditText)findViewById(R.id.editText_Ort);
        plz=(EditText)findViewById(R.id.editText_PLZ);
        dauerVon = (EditText)findViewById(R.id.editText_Von);
        dauerBis =(EditText)findViewById(R.id.editText_Bis);
        preis=(EditText)findViewById(R.id.editText_Preis);

        zurück = findViewById(R.id.button_zurück);
        zurück.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
            }
        });

        speichern= findViewById(R.id.button_speichern);
        speichern.setOnClickListener(view->saveItem());
        löschen = findViewById(R.id.button_delete);
        löschen.setVisibility(View.INVISIBLE);

        imageViewCity=findViewById(R.id.imageView2);
        imageViewCity.setOnClickListener((view->selectImage()));

        angebot=(Angebot) getIntent().getSerializableExtra("angebot");
        if(angebot != null){
            String Adresse = angebot.getAdresse();
            String Strasse = Adresse.substring(0,Adresse.indexOf(","));
            String Ort = Adresse.substring(Adresse.indexOf(",")+1,Adresse.indexOf("--"));
            String Plz = Adresse.substring(Adresse.indexOf("--")+2,Adresse.length()-1);


            adresse.setText( Strasse);
            ort.setText(Ort);
            plz.setText(Plz);
            preis.setText(angebot.getPreis());
            dauerVon.setText(angebot.getDauerVon());
            dauerBis.setText(angebot.getDauerBis());
            imageUri = angebot.getImageUri();
            Uri uri = Uri.parse(imageUri);
            if(uri != null){
                imageViewCity.setImageURI(uri);
            }
            löschen.setVisibility(View.VISIBLE);
            löschen.setOnClickListener(view->deleteItem());
        }
        db= DatabaseClient.getInstance(getApplicationContext()).getAppDatabase();


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main1, menu);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int itemId = item.getItemId() ;

        if(itemId == R.id.back ){
            Intent myIntent =  new Intent(this , MainActivity.class) ;
            startActivity(myIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveItem(){
        if (angebot == null) {
            angebot = new Angebot();
        }

        if(adresse.getText().toString().isEmpty())
        {
            adresse.setError("Die Strasse ist angefordert!");
            adresse.requestFocus();
            return;
        }
        if(ort.getText().toString().isEmpty())
        {
            ort.setError("Der Ort ist angefordert!");
            ort.requestFocus();
            return;
        }
        if(plz.getText().toString().isEmpty())
        {
            plz.setError("Die Postleitzahl ist angefordert!");
            plz.requestFocus();
            return;
        }
        if(dauerVon.getText().toString().isEmpty())
        {
            dauerVon.setError("Das Datum ist angefordert!");
            dauerVon.requestFocus();
            return;
        }
        if(dauerBis.getText().toString().isEmpty())
        {
            dauerBis.setError("Das Datum ist angefordert!");
            dauerBis.requestFocus();
            return;
        }
        if(preis.getText().toString().isEmpty())
        {
            preis.setError("Der Preis ist angefordert!");
            preis.requestFocus();
            return;
        }

        angebot.setAdresse(adresse.getText().toString().trim()+", "+ort.getText().toString().trim()+"--"+plz.getText().toString().trim());
        angebot.setPreis(preis.getText().toString());
        angebot.setDauerVon(dauerVon.getText().toString());
        angebot.setDauerBis(dauerBis.getText().toString());

        angebot.setImageUri(imageUri);
        if (angebot.getUid() > 0)
            new Thread(() -> {db.angebotDao().update(angebot);}).start();
        else
            new Thread(() -> {db.angebotDao().insert(angebot);}).start();
        finish();
    }

    private void deleteItem() {
        if (angebot != null) {
            new Thread(() -> {
                db.angebotDao().delete(angebot);
                MainActivityTicketVerkauf.this.runOnUiThread(() -> Toast.makeText(MainActivityTicketVerkauf.this, "gelöscht", Toast.LENGTH_LONG).show());
            }).start();

        }
        finish();
    }


    private void selectImage(){

        Intent gallery = new Intent(Intent.ACTION_OPEN_DOCUMENT, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, REQUEST_PICK_IMAGE);
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_PICK_IMAGE && data != null){
            Uri uri = data.getData();
            if (uri != null) {
                //sonst nach Neustart nicht mehr verfügbar ...
                getContentResolver().takePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                imageViewCity.setImageURI(uri);
                imageUri = uri.toString();
            }
        }
    }

}
