package com.example.parkplatz;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Angebot.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    public abstract AngebotDao angebotDao();
}
