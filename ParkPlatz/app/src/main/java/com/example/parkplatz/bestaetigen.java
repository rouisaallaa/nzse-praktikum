package com.example.parkplatz;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class bestaetigen extends AppCompatActivity {
    private Angebot angebot = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bestaetigen);
        angebot = (Angebot) getIntent().getSerializableExtra("angebot");
        Button btnReservieren = (Button) findViewById(R.id.BB1);
        Button btnNichtReservieren = (Button) findViewById(R.id.BB2);


        btnReservieren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase().angebotDao().delete(angebot);

                Intent intent = new Intent(bestaetigen.this , KaufUebersicht.class) ;
                intent.putExtra("angebot", angebot);
                startActivity(intent);
                Toast.makeText(bestaetigen.this,"Das Ticket wurde erfolgreich gebucht.", Toast.LENGTH_LONG).show();
            }
        });

        btnNichtReservieren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(bestaetigen.this , MainActivityTicketKauf.class));
            }
        });
    }
}
