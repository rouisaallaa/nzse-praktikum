package com.example.parkplatz;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.AngeboteViewHolder>{

    private List<Angebot> angebote;
    private Context context;

    public TicketAdapter(Context context, List<Angebot> angebote) {
        this.context = context;
        this.angebote = angebote;
    }

    @Override
    public TicketAdapter.AngeboteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_holder_angebot, parent, false);
        return new TicketAdapter.AngeboteViewHolder(view);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(AngeboteViewHolder holder, int position) {

        Angebot angebot = angebote.get(position);
        holder.textViewInhabitants.setText( angebot.getAdresse());
        holder.textViewDescription.setText(String.format("Höhe der Miete: %s € ", angebot.getPreis()));

        holder.imageViewCity.setImageURI(Uri.parse(angebot.getImageUri()));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return angebote.size();
    }

    class AngeboteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textViewInhabitants, textViewDescription;
        ImageView imageViewCity;

        public AngeboteViewHolder(View itemView) {
            super(itemView);


            textViewInhabitants = itemView.findViewById(R.id.adresseTextView);
            textViewDescription = itemView.findViewById(R.id.preis_textView);
            imageViewCity = itemView.findViewById(R.id.imageView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Angebot angebot = angebote.get(getAdapterPosition());

            Intent intent = new Intent(context, MainActivityTicketVerkauf.class);
            intent.putExtra("angebot", angebot);

            ((Activity) context).startActivity(intent);
        }

    }
}
