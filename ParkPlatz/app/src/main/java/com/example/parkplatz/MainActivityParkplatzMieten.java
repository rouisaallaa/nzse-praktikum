package com.example.parkplatz;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.List;

public class MainActivityParkplatzMieten extends AppCompatActivity {
    private RecyclerView recyclerView;
    private List<Angebot> allAngebote;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        recyclerView = findViewById(R.id.Recycler_Angebot);
        //Layout für die Anordnung der Elemente
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void fillAngebote() {
        List<Angebot> filteredAngebote;

        filteredAngebote=allAngebote;

        recyclerView.setAdapter(new AngebotAdapter2(this, filteredAngebote));

    }

    @Override
    protected void onResume(){
        super.onResume();
        allAngebote = DatabaseClient.getInstance(getApplicationContext()).getAppDatabase().angebotDao().getAll();
        fillAngebote();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main2, menu);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int itemId = item.getItemId() ;

        if(itemId == R.id.back ){
            Intent myIntent =  new Intent(this , MainActivity.class) ;
            startActivity(myIntent);
        }

        return super.onOptionsItemSelected(item);
    }
}
