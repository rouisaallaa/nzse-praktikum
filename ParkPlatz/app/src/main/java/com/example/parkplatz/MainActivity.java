package com.example.parkplatz;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int itemId = item.getItemId() ;

        if(itemId == R.id.login_back ){
            Intent myIntent =  new Intent(this , login.class) ;
            startActivity(myIntent);
            Toast.makeText(this,"You  are logged out successfully.", Toast.LENGTH_LONG).show();

        }

        return super.onOptionsItemSelected(item);
    }
    public void icon(View view) {
        Intent myIntent =  new Intent(this , SettingsAndCredits.class) ;
        startActivity(myIntent);
    }

    public void f1(View view) {
        Intent myIntent =  new Intent(this , Details_TicketVerkaufen.class) ;
        startActivity(myIntent);
    }
    public void f2(View view) {
        Intent myIntent =  new Intent(this , MainActivityTicketKauf.class) ;
        startActivity(myIntent);
    }
    public void f3(View view) {
        Intent myIntent =  new Intent(this , MainActivityParkplatzVermieten.class) ;
        startActivity(myIntent);
    }
    public void f4(View view) {
        Intent myIntent =  new Intent(this , MainActivityParkplatzMieten.class) ;
        startActivity(myIntent);
    }
}
