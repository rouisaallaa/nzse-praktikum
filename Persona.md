**Persona 1**

Max Mustermann
Alter: 22
Beruf: Student
Wohnort: Darmstadt
Autofahren: 3-4 mal pro Woche
Bio: 
Max ist 22 Jahre alt und studiert Maschinenbau an der TU Darmstadt. 
Es nervt ihn mit der Zeit, immer zu viel Geld für sein Parkticket auszugeben, wenn er nie die komplette Zeit ausnutzt. 
Generell benutzt er sein Auto nur in Darmstadt und wenn dann auch meistens, wenn er zur Uni fährt. 
Wenn er ein Parkticket über die App finden will, dann sollte das sehr schnell gehen und vor allem digital erfolgen. 
Erst ewig lang die Person, welche ihm das Parkticket geben will zu suchen, würde die Nutzung der App für ihn sehr unangenehm gestalten. 
Ein wichtiger Punkt für ihn ist deshalb, dass er sein Parkticket mobil über die App erhalten kann und dieses auch 
selbst einfach einscannen kann, sodass er keine Arbeit damit hat, wenn er jemandem sein Ticket geben will.

Goals: 

•	Parktickets sehr schnell erwerben und tauschen

•	Digital in der App erhältliches Parkticket

Frustrations:

•	langes Suchen nach der Person, welche das Parkticket hat

•	Laufwege, um zur Person zu gelangen

**Persona 2**

Name: Florian Müller
Alter: 23
Beruf: Student
Wohnort: Darmstadt
Autofahren: 1 mal alle 2 Wochen
Bio: 
Florian ist 23 Jahre alt, studiert an der Hochschule und benutzt sein Auto sehr wenig.
 Meistens fährt er mit dem Fahrrad zur Schule und hat deswegen wenig Probleme bei der Parkplatzsuche. 
Wenn er jedoch mal mit dem Auto fährt, wäre er bereit ein bisschen Zeit zu investieren, um weniger Geld für das Parkticket zu bezahlen.
 Es würde ihm reichen, wenn er in der App sehen würde, wo die Person momentan ist und er das Parkticket einfach abholen könnte. 
Dafür würde er auch etwas Laufweg in Kauf nehmen.

Goals:

•	Geld beim Parken sparen

•	Seinen Mitmenschen helfen, ein wenig Geld zu sparen

Frustrations:

•	Keine Person zum Parktickettausch in der Nähe finden

